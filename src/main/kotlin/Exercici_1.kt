import java.util.*

/*
    * NOM: Erik Vasilyan
    * DATA: 2023-03-06
    * TITOL: Road Signs
 */

fun main() {
    val scanner = Scanner(System.`in`)

    val cartells = mutableMapOf<Int, String>()

    print("Quants cartells vols introduir: ")
    val numeroCartells = scanner.nextInt()

    for (i in 1 .. numeroCartells) {
        val metre = scanner.nextInt()
        val cartell = scanner.next()
        cartells.put(metre, cartell)
    }

    print("\nQuants consultes vols fer: ")
    val nombreConsultes = scanner.nextInt()

    for (j in 1 .. nombreConsultes) {
        val metre = scanner.nextInt()

        if (cartells.containsKey(metre)) {
            val cartell = cartells.getValue(metre)
            println(cartell)
        }
        else println("No hi ha cartell")
    }
}