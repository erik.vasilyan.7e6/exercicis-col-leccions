import java.util.*

/*
    * NOM: Erik Vasilyan
    * DATA: 2023-03-13
    * TITOL: Repeated Answer
 */

fun main() {
    val scanner = Scanner(System.`in`)

    val respostes = mutableSetOf<String>()

    do {
        val resposta = scanner.next()

        if (resposta != "END") {
            if (!respostes.contains(resposta)) respostes.add(resposta)
            else println("MEEEC!")
        }
    } while (resposta != "END")
}