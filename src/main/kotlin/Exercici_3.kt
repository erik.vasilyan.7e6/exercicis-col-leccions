import java.util.*

/*
    * NOM: Erik Vasilyan
    * DATA: 2023-03-10
    * TITOL: School Delegat Voting
 */

fun main() {
    val scanner = Scanner(System.`in`)

    val vots = mutableListOf<String>()

    do {
        val nom = scanner.next()

        if (nom != "END") vots.add(nom)

    } while (nom != "END")

    val votsAgrupats = vots.groupingBy { it }.eachCount()

    votsAgrupats.forEach {
        println("${it.key}: ${it.value}")
    }
}