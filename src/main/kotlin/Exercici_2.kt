import java.util.*

/*
    * NOM: Erik Vasilyan
    * DATA: 2023-03-06
    * TITOL: Employee By Id
 */

data class Empleat (val dni: String, val nom: String, val cognoms: String, val adreca: String)

fun main() {
    val scanner = Scanner(System.`in`)

    val empleats = mutableMapOf<String, Empleat>()

    print("Quantitat d'empleats: ")
    val quantitatEmpleats = scanner.nextInt()

    for (i in 1 .. quantitatEmpleats) {
        print("DNI: ")
        val dni = scanner.next()

        print("Nom: ")
        val nom = scanner.next()

        print("Cognoms: ")
        val cognoms = scanner.next()

        print("Adreça: ")
        scanner.nextLine()
        val adreca = scanner.nextLine()

        val empleat = Empleat(dni, nom, cognoms, adreca)
        empleats.put(dni, empleat)
    }

    var dniEmpleat: String
    do {
        dniEmpleat = scanner.next()

        if (dniEmpleat != "END") {
            val empleatEncontrat = empleats.getValue(dniEmpleat)
            println("${empleatEncontrat.nom} ${empleatEncontrat.cognoms} - ${empleatEncontrat.dni}, ${empleatEncontrat.adreca}")
        }

    } while (dniEmpleat != "END")
}