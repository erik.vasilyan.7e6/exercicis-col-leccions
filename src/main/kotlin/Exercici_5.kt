import java.util.*

/*
    * NOM: Erik Vasilyan
    * DATA: 2023-03-13
    * TITOL: Bingo
 */

fun main() {
    val scanner = Scanner(System.`in`)
    var repeat = true

    var n = 10
    val targeta = mutableSetOf<Int>()

    println("Números de la targeta")
    println("----------------------")

    for (i in 1 .. 10) {
        val numero = scanner.nextInt()
        targeta.add(numero)
    }

    println("Canta números")
    println("----------------------")

    while (repeat) {
        val numeroCantat = scanner.nextInt()

        if (targeta.contains(numeroCantat)) {
            n--
            if (n == 0) {
                repeat = false
            }
        }
        if (n != 0) println("Em queden $n números")
    }

    println("BINGO")
}